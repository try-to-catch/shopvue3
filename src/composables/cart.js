import {computed, ref} from "vue";

const cart = ref([])

export function useCart() {
    const isProductInCart = (product) => {
        return cart.value.some(item => {
            return item.id === product.id
        })
    }

    function addToCart(product) {
        if (isProductInCart(product)) return
        cart.value.push(product)
    }

    const getTotal = computed(() => {
        return cart.value.reduce((acc, item) => {
            return acc + item.price
        }, 0)
    })


    const getCart = computed(() => {
        return cart.value
    })

    return {addToCart, getCart, getTotal}
}